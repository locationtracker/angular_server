// Import dependencies
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

// MongoDB URL from the docker-compose file
const dbHost = 'mongodb://database/mean-docker';

// Connect to mongodb
mongoose.connect(dbHost);

// create mongoose schema
const locationSchema = new mongoose.Schema({
  lat: Number,
  lon: Number,
  timestamp: Number,
  speed: Number,
});

// create mongoose model
const Location = mongoose.model('Location', locationSchema);

/* GET api listing. */
router.get('/', (req, res) => {
        res.send('api works');
});

/* GET all locations. */
router.get('/locations', (req, res) => {
    Location.find({}, (err, locations) => {
        if (err) res.status(500).send(error)

        res.status(200).json(locations);
    });
});

/* GET one locations. */
router.get('/locations/:id', (req, res) => {
    Location.findById(req.param.id, (err, locations) => {
        if (err) res.status(500).send(error)

        res.status(200).json(locations);
    });
});

/* Create a location. */
router.post('/locations', (req, res) => {
    let location = new Location({
        lat: req.body.lat,
        lon: req.body.lon,
        speed: req.body.speed,
        timestamp: req.body.timestamp
    });

    location.save(error => {
        if (error) res.status(500).send(error);

        res.status(201).json({
            message: 'Location created successfully'
        });
    });
});

module.exports = router;
